FROM node:16-alpine@sha256:a1f9d027912b58a7c75be7716c97cfbc6d3099f3a97ed84aa490be9dee20e787 as build

WORKDIR /app

COPY package*.json ./
COPY webpack.config.js ./
COPY /src/ ./src/
RUN npm install --include=prod &&\
    npm run build &&\
    npm cache clean --force

FROM nginx:1.27-alpine-slim@sha256:66943ac4a1ca7f111097d3c656939dfe8ae2bc8314bb45d6d80419c5fb25e304

WORKDIR /usr/share/nginx/html
COPY --from=build /app/dist .